/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Outfit', 'sans-serif'],
        // Add or modify font families here
      },
      height:{
        'h-100' : '26em'
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      backgroundColor:{
        'dark-blue' : '#10141E',
        'red' : '#FC4747',
        'greyish-blue': '#5A698F',
        'semi-dark-blue' : '#161D2F'
      }
    },
  },
  plugins: [
    require('tailwind-scrollbar-hide')
    // ...
  ]}
