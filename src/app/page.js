'use client'
import React, { useState } from 'react';
import Image from 'next/image';
import SearchIcon from '../../public/assets/icon-search.svg';
import Datas from '../../public/data.json';
import SwiperComponent from '../components/sliders/Sliders';
import MovieIcon from '../../public/assets/icon-category-movie.svg';
import TvIcon from '../../public/assets/icon-category-tv.svg';
import BookMarked from '@/components/Bookmark/BookMarkOn';
import BookmMarkEmpty from '@/components/Bookmark/Bookmark';


const Home = () => {
  const [isSearch, setIsSearch] = useState('');

  const filterMovies = Datas.filter((data) =>
    data.title.toLowerCase().includes(isSearch.toLowerCase())
  );

  const handleChange = (e) => {
    setIsSearch(e.target.value);
  };

  const searchResultCount = filterMovies.length;

  const renderSearchResults = () => {
    return (
      <h2 className="ms-5 text-2xl mt-8 text-white font-extralight">
        Found {searchResultCount} results for {isSearch}
      </h2>
    );
  };

  return (
    <main className="mx-auto lg:max-w-6xl lg:mx-auto lg:mx-0 ">
      <div className="flex max-w-lg items-center">
        <Image className="ms-4 me-4 mt-4 w-9 h-9" alt="search" src={SearchIcon} />
        <input
          onChange={handleChange}
          type="text"
          className="bg-transparent w-96 text-slate-100 border-blue-50 pt-4 border-b-2 border-none outline-none"
          placeholder="Search for movies or TV series"
        />
      </div>
      {isSearch ? (
        renderSearchResults()
      ) : (
        <>
          <h2 className="ms-5 text-2xl mt-8 text-white font-extralight">Trending</h2>
          <div className="mt-5 ms-3">
            <SwiperComponent />
          </div>
          <h2 className="ms-5 text-2xl mt-8 text-white font-extralight">Recomended for you</h2>
        </>
      )}

      {
        isSearch ? (
          <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-3 px-4 mt-5">
          {filterMovies
            .map((data) => {
              const regularMovieAndTv = data.thumbnail.regular.small; // Slicing the image URL
              return (
                <div key={data.id}>
                  <img className="rounded-lg" src={regularMovieAndTv} alt={data.title} />
                  <p className="text-slate-500 mb-2 text-sm">
                    {data.year}
                    {data.category === 'Movie' && <Image className="inline-block mx-3" src={MovieIcon} />}
                    {data.category === 'TV Series' && <Image className="inline-block mx-3" src={TvIcon} />}
                    <span className="">{data.category}</span>
                    <span className="ms-3">{data.rating}</span>
                  </p>
                  <p className="text-white md:font-extrabold md:text-3xl mb-5">{data.title}</p>
                </div>
              );
            })}
        </div>      
        ) : (
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-3 px-4 mt-5">
          {filterMovies
            .filter((data) => data.id >= 6)
            .map((data) => {
              const slicedThumbnail = data.thumbnail.regular.small; // Slicing the image URL
              const movieCategory =  data.category === 'Movie';
              const tvCategory = data.category === 'TV Series';

              return (
                <div className='relative' key={data.id}>
                  <img className="rounded-lg" src={slicedThumbnail} alt={data.title} />
                  
                  {data.isBookmarked ? <BookMarked/> : <BookmMarkEmpty/>}

                  <p className="text-slate-500 mb-2 md:text-lg text-sm">
                    {data.year}
                    { movieCategory && <Image className="inline-block mx-3 md:h-4 md:w-4" src={MovieIcon} />}
                    { tvCategory&& <Image className="inline-block mx-3 md:h-4 md:w-4" src={TvIcon} />}
                    <span className="md:text-lg">{data.category}</span>
                    <span className="ms-3 md:text-lg">{data.rating}</span>
                  </p>
                  <p className="text-white mb-5 font-extrabold md:text-xl">{data.title}</p>
                </div>
              );
            })}
        </div>      
        )
      }

      
        
    </main>
  );
};

export default Home;
