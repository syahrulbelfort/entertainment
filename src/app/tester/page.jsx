import React from 'react';
import Grid from '@/components/Grids/grid';
import Datas from '../../../public/data.json';

export default function Test() {
  return (
    <>
      <Grid movies={Datas} />
    </>
  );
}
