'use client'
import React, { useState } from 'react';
import Image from 'next/image';
import Datas from '../../../public/data.json';
import SearchIcon from '../../../public/assets/icon-search.svg';
import MovieIcon from '../../../public/assets/icon-category-movie.svg';
import BookMarked from '@/components/Bookmark/BookMarkOn';
import BookMarkEmpty from '@/components/Bookmark/Bookmark';

const TvSeries = () => {
  const [isSearch, setIsSearch] = useState('');

  const filterMovies = Datas.filter((data) =>
    data.title.toLowerCase().includes(isSearch.toLowerCase())
  );

  const handleChange = (e) => {
    setIsSearch(e.target.value);
  };

  const searchResultCount = filterMovies.length;

  const renderSearchResults = () => {
    return (
        
      <h2 className="ms-5 text-2xl mt-8 text-white font-extralight">
        Found {searchResultCount} results for {isSearch}
      </h2>
    );
  };

  return (
    <main className="mx-auto">
      <div className="flex max-w-lg items-center">
        <Image className="ms-4 me-4 mt-4 w-9 h-9" alt="search" src={SearchIcon} />
        <input
          onChange={handleChange}
          type="text"
          className="bg-transparent w-96 text-slate-100 border-blue-50 pt-4 border-b-2 border-none outline-none"
          placeholder="Search for TV Series"
        />
      </div>
      
       { isSearch && renderSearchResults()}
       <h2 className="ms-5 text-2xl mt-8 text-white font-extralight">TV Series</h2>

        
        <div className="grid grid-cols-2  md:grid-cols-3 lg:grid-cols-4 gap-3 px-4 mt-5">
          {filterMovies
            .filter((data) => data.category === 'TV Series')
            .map((data) => {
              const slicedThumbnail = data.thumbnail.regular.small; // Slicing the image URL
              const tvSeriesCategory =  data.category === 'TV Series';

              return (
                <div className='relative' key={data.id}>
                  <img className="rounded-lg" src={slicedThumbnail} alt={data.title} />
                  
                  {data.isBookmarked ? <BookMarked/> : <BookMarkEmpty/>}

                  <p className="text-slate-500 mb-2 md:text-lg text-sm">
                    {data.year}
                    { tvSeriesCategory && <Image className="inline-block mx-3" src={MovieIcon} />}
                    <span className="md:text-lg">{data.category}</span>
                    <span className="ms-3 md:text-lg">{data.rating}</span>
                  </p>
                  <p className="text-white mb-5 font-extrabold md:text-xl">{data.title}</p>
                </div>
              );
            })}
        </div>           
        
    </main>
  );
};

export default TvSeries;
