
import Image from 'next/image';
import Logo from '../../../public/assets/logo.svg';

export default function Signup() {
  return (
    <>
      <div className="max-w-sm mx-auto">
        <div>
           <Image className="mx-auto mt-14 w-14" src={Logo} alt="Logo" />
        </div>
        <div className=" mx-auto mt-10 w-98  bg-semi-dark-blue py-20 rounded-md ">
        <form className="flex flex-col gap-5 px-10 ">
            <label className="text-white text-4xl font-normal mb-8">Sign Up</label>
            <input type="text" className="py-5 bg-transparent text-slate-100 ps-2	 border-blue-50 border-b-2 border-slate-700	outline-none " placeholder="Email address" />
            <input type="password" className="py-5 bg-transparent border-b-2 text-slate-100 ps-2 border-slate-700 outline-none		" placeholder="Password" />
            <input type="password" className="py-5 bg-transparent	border-b-2 text-slate-100 ps-2 border-slate-700	outline-none" placeholder="Repeat Password"/>
            <button type='submit' className='bg-red py-4 rounded-lg text-white '>Create an account</button>
          </form>
          <p className="text-white  text-center mt-10">Already have an account? <a href='/login'><span className="text-red-500">Login</span></a></p>
        </div>
      </div>
   
    </>
  );
}

