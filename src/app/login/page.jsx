import React from 'react';
import Image from 'next/image';
import Logo from '../../../public/assets/logo.svg';
import Form from '@/components/Form';

export default function login() {
  return (
    <>
        <Form
        image={Logo}
        label={'Sign In'}
        question={'Dont have an account?'}
        action={'Sign Up'}
        link={'/signup'}
        />
    </>
  );
}
