"use client"

import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { FreeMode } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "./styles.css";
import Datas from '../../../public/data.json';
import Bookmark from "../Bookmark/Bookmark";

// Initialize Swiper modules
SwiperCore.use([FreeMode]);

export default function SwiperComponent() {
  return (
    <Swiper
      slidesPerView={1.42}
      spaceBetween={20}
      freeMode={true}
      modules={[FreeMode]}
      className="mySwiper"
    >
       {Datas.map((data) => {
          return (
            data.isTrending && (
              <SwiperSlide className="relative  ">
                <h4 className="absolute bottom-8 md:left-28 text-sm md:text-md left-24 text-white">{data.category}</h4>
                <h4 className="absolute bottom-8 md:left-12 text-sm md:text-md left-6 text-white">{data.year}</h4>
                <h4 className="absolute bottom-2  md:left-12 left-6 text-white">{data.title}</h4>
                <Bookmark/>

              <img
                className="ms-5"
                src={data.thumbnail.trending.large}
                alt={data.title}
              />

         
              </SwiperSlide>
              
            )
          );
        })}
    </Swiper>
  );
}
