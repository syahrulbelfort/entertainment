import React from 'react';
import Image from 'next/image';
import Logo from '../../public/assets/logo.svg';
import Home from '../../public/assets/icon-nav-home.svg';
import Movie from '../../public/assets/icon-nav-movies.svg';
import Tv from '../../public/assets/icon-nav-tv-series.svg';
import Bookmark from '../../public/assets/icon-nav-bookmark.svg';
import Avatar from '../../public/assets/image-avatar.png';
import Link from 'next/link';

export default function Navbar() {
  return (
    <>
      <div className="bg-semi-dark-blue lg:rounded-3xl  lg:mt-2 p-4  lg:fixed lg:z-10 ">
        <div className="flex justify-between lg:flex-col lg:gap-52  items-center">
          <Link href="/login">
            <Image className="lg:w-8 lg:h-8" src={Logo} />
          </Link>
          <div className="flex lg:flex-col lg:gap-10 gap-6 ">
            <Link href="/">
              <Image className="lg:w-8 lg:h-8" src={Home} />
            </Link>
            <Link href="/movies">
              <Image className="lg:w-8 lg:h-8" src={Movie} />
            </Link>
            <Link href="/tvseries">
              <Image className="lg:w-8 lg:h-8" src={Tv} />
            </Link>
            <Link href="/bookmark">
              <Image className="lg:w-8 lg:h-8" src={Bookmark} />
            </Link>
          </div>
          <Image className="w-8 h-8" src={Avatar} />
        </div>
      </div>
    </>
  );
}
