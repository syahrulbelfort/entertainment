import React from 'react';
import Image from 'next/image';

export default function Grid(props) {
  return (
    <div className="grid grid-cols-2 md:grid-cols-3 gap-3 px-4 mt-5">
      {props.movies.map((data) => {
        const slicedThumbnail = data.thumbnail.regular.small; // Slicing the image URL
        const movieCategory = data.category === 'Movie';
        const tvCategory = data.category === 'TV Series';

        return (
          <div className='relative' key={data.id}>
            <img className="rounded-lg" src={slicedThumbnail} alt={data.title} />

            {/* {data.isBookmarked ? <BookMarked/> : <BookMarkEmpty/>} */}

            <p className="text-slate-500 mb-2 md:text-lg text-sm">
              <Image src='' />
              <Image src='' />
              <span className="md:text-lg">{data.category}</span>
              <span className="ms-3 md:text-lg">{data.rating}</span>
            </p>
            <p className="text-white mb-5 md:font-extrabold text-xl">{data.title}</p>
          </div>
        );
      })}
    </div>
  );
}
