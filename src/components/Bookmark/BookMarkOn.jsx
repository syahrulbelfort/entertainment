import Image from 'next/image'
import React from 'react'
import BookMarkOn from '../../../public/assets/icon-bookmark-full.svg'

export default function BookMarked() {
  return (
    <>    
        <div className="absolute bg-slate-600 rounded-full  p-3 top-2 right-2 ">
            <Image
            className="w-2 h-2 "
            src={BookMarkOn}
            />
         </div>
    </>
  )
}
