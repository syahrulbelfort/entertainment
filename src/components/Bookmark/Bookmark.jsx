import React from 'react'
import BookMarkEmpty from '../../../public/assets/icon-bookmark-empty.svg'
import Image from 'next/image'

export default function Bookmark() {


  return (
   
    <>
        <div className="absolute bg-slate-600 opacity-50 rounded-full  p-3 top-2 right-2 ">
         <Image
        className="w-2 h-2  "
        src={BookMarkEmpty}
           />
     </div>
    </>
  )
}
